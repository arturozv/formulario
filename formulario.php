<?php
//INICIAMOS LA SESION Y COMPROBAMOS SI HAY UNA SESION ABIERTA, EN CASO CONTRARIO NOS REGRESA AL LOGIN
session_start();
$varsesion = $_SESSION['Alumno'];
if($varsesion == null || $varsesion = ''){
	header("Location:login.php");
	die();
}
?>

<html>
<head>
    <title>Formulario</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
	<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
  
     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
	<script src="https://kit.fontawesome.com/96e7573428.js" crossorigin="anonymous"></script>
	<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
    
  
</head>
<body>
	<div class="container">
		
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			<button class="navbar-toggler" data-target="#menu" data-toggle="collapse" type="button">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="menu">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active"><a href="info.php" class="nav-link">Home</a></li>
				<li class="nav-item active"><a href="formulario.php" class="nav-link">Registrar alumnos</a></li>
				<li class="nav-item active"><a href="cerrar.php" class="nav-link">Cerrar sesion</a></li>
			</ul>
			</div>
		</nav>
</br>
	<div class="row">
		<div class="col-lg-10">
		<div class="card shadow-lg p-6 mb-8 bg-white">
		<div class="card-body">

		<form action="<?php echo 
				htmlspecialchars($_SERVER['PHP_SELF'])?>" method="POST">
				
					<!-- Numero de cuenta -->
				<div class="form-row">
					<div class="col-md-4 mb-3">
						<label class="form-label" for="numcuenta">Numero de cuenta</label>
						<input type="text" id="numcuenta" name="num_cuenta" class="form-control">
					</div>
				</div>
					<!-- Nombre -->
				<div class="form-row">
					<div class="col-md-4 mb-3">
						<label class="form-label" for="nombre">Nombre</label>
						<input name="nombre" class="form-control " type="text" id="nombre" placeholder="">
					</div>
				</div>
						<!-- Primer Apellido -->
				<div class="form-row">
					<div class="col-md-4 mb-3">
					<label class="form-label" for="primer">Primer Apellido</label>
					<input name="primer_apellido" class="form-control" type="text" id="primer" placeholder="">
					</div>
				</div>
					<!-- Segundo Apellido -->
				<div class="form-row">
					<div class="col-md-4 mb-3">
					<label class="form-label" for="segundo">Segundo Apellido</label>
					<input name="segundo_apellido" class="form-control" type="text" id="segundo" placeholder="">
					</div>
				</div>
					<!-- Genero -->
				<div class="form-row">
					<div class="col-md-4 mb-3">
					<label class="form-label">Genero</label>
					<label class="form-radio">
						<input type="radio" name="genero" value="H" checked>
						<i class="form-icon"></i> Hombre
					</label>
					<label class="form-radio">
						<input type="radio" name="genero" value="M">
						<i class="form-icon"></i> Mujer
					</label>
					<label class="form-radio">
						<input type="radio" name="genero" value="O">
						<i class="form-icon"></i> Otro
					</label>
					</div>
				</div>
					<!-- Fecha de Nacimiento -->
				<div class="form-row">
					<div class="col-md-4 mb-3">
					<label class="form-label" for="fechanac">Fecha de Nacimiento</label>
					<input name="fecha_nac" class="form-control " type="date" id="fechanac" placeholder="">
					</div>
				</div>
					<!-- Contraseña -->
				<div class="form-row">
					<div class="col-md-4 mb-3">
					<label class="form-label" for="contrasena">Contraseña</label>
					<input name="contrasena" class="form-control" type="password" id="contrasena">
					</div>
				</div>
					<!-- Botones -->
					<button type='submit' name="submit" class="btn btn-primary">Enviar</button>
				</form>
		</div>
		</div>
		</div>
	</div>

		<?php
		//ESTA FUNCION SE EJECTURA UNA VEZ ENVIADA LA INFORMACION DEL NUEVO REGISTRO
		if(isset($_POST['submit'])){
			$num_cuenta = $_POST['num_cuenta'];
			$nombre = $_POST['nombre'];
			$primer_apellido = $_POST['primer_apellido'];
			$segundo_apellido = $_POST['segundo_apellido'];
			$genero = $_POST['genero'];
			$fecha_nac = $_POST['fecha_nac'];
			$contrasena = $_POST['contrasena'];
			
			//INTENTE HACERLA CON PUSH_ARRAY PERO ME CREABA OTROS INDICES EN LUGAR DE OCUPAR EL DE NUM_CUENTA, NOMBRE, ETC
			$_SESSION['Alumno'][$num_cuenta]['num_cuenta']=$num_cuenta;
			$_SESSION['Alumno'][$num_cuenta]['nombre']=$nombre;
			$_SESSION['Alumno'][$num_cuenta]['primer_apellido']=$primer_apellido;
			$_SESSION['Alumno'][$num_cuenta]['segundo_apellido']=$segundo_apellido;
			$_SESSION['Alumno'][$num_cuenta]['contrasena']=$contrasena;
			$_SESSION['Alumno'][$num_cuenta]['genero']=$genero;
			$_SESSION['Alumno'][$num_cuenta]['fecha_nac']=$fecha_nac;

			//print_r($_SESSION['Alumno']);
			//header("Location:info.php"); ANTES ME FUNCIONABA LA REDIRECCION, DESPUES DE AGREGARLE DISEÑO ME MARCABA ERROR
		}?>
	</div>
    </body>
</html>