<?php 
//INICIAMOS LA SESION Y COMPROBAMOS SI HAY UNA SESION ABIERTA, EN CASO CONTRARIO NOS REGRESA AL LOGIN
session_start();
$varsesion = $_SESSION['Alumno'];
if($varsesion == null || $varsesion = ''){
	header("Location:login.php");
	die();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
	<title>Info</title>
     <!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
	<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
  
     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
	<script src="https://kit.fontawesome.com/96e7573428.js" crossorigin="anonymous"></script>
	<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
    
</head>
<body>
<div class="container">
     <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
          <button class="navbar-toggler" data-target="#menu" data-toggle="collapse" type="button">
               <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="menu">
          <ul class="navbar-nav mr-auto">
               <li class="nav-item active"><a href="info.php" class="nav-link">Home</a></li>
               <li class="nav-item active"><a href="formulario.php" class="nav-link">Registrar alumnos</a></li>
               <li class="nav-item active"><a href="cerrar.php" class="nav-link">Cerrar sesion</a></li>
          </ul>
          </div>
     </nav>
</br>
     <!-- Informacion del alumno -->
		<div class="card shadow-lg p-6 mb-8 bg-white">
		<div class="card-body">
	<h3>Usuario Autenticado</h3>
		<?php
			//print_r($_SESSION['Alumno']);
			
			echo "<p>Bienvenido ".$_SESSION['Alumno'][1]['nombre']." ".$_SESSION['Alumno'][1]['primer_apellido']."</p>";
		?>
		<h3>Informacion</h3>
               <?php
               // YA NO PUDE OBTENER EL INDICE DEL ALUMNO QUE HAYA INGRESADO, ASI QUE SIEMPRE APARECERAN LOS DATOS DEL ADMIN
			echo "<p>Numero de cuenta: ".$_SESSION['Alumno'][1]['num_cuenta']."</p>";
			echo "<p>Fecha de Nacimiento: ".$_SESSION['Alumno'][1]['fecha_nac']."</p>";
			?>
          </div>
          </div>
          
<!-- TABLA DE ALUMNOS -->
	<div class="card-body">
		<h3 class="card-title">Datos Guardados:</h3>
		<table id="listado-alumnos" class="table table-dark table-hover table-bordered">
                    <thead>
                         <tr>
                              <th scope="col">#</th>
                              <th scope="col">Nombre</th>
                              <th scope="col">Fecha de Nacimiento</th>
                         </tr>
                    </thead>

                    <tbody>
                         <?php  
                                    
                                   foreach($_SESSION['Alumno'] as $alumno) { ?>
                                   <tr>
                                        
                                        <td><?php echo $alumno['num_cuenta']; ?></td>
                                        <td><?php echo $alumno['nombre']." ".$alumno['primer_apellido']; ?></td>
                                        <td><?php echo $alumno['fecha_nac']; ?></td>
                                        
                                   </tr>
                                   <?php }
                               ?>
                         
                    </tbody>
               </table>

	</div>

	
</div>
</body>
</html>

